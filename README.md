# Nursoft
## Prueba Técnica

## Info
- Ruby 2.3.1
- Rails 5.1.4
- Postgres

## Instalación
```
$ bundle install
$ bundle exec figaro install
```
## Configuración BD Local

Crear BD Postgres:
```
$ createdb -Otunombredeusuario -Eutf8 nursoft-test
```

Configurar `config/application.yml` añadiendo:
```
development:
  DBNAME: nursoft-test
  DBUSER: tunombreusuario
  DBPASS: tucontraseña

test:
  DBNAME: nursoft-test
  DBUSER: tunombreusuario
  DBPASS: tucontraseña
```

Correr Migraciones y Seed
```
rake db:migrate
rake db:seed
```
