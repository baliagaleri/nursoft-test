window.onload = function () {
  var employees = new Vue({
    el: '#indicators',
    data: {
      indicator_types: [],
      indicator_type_id: '',
      title: '',
      last_value: '',
      last_date: '',
      start_date: '',
      final_date: '',
      predict_value: '',
      estimation_error: '',
      correlation_coefficient: '',
      search_indicator_values: [],
      is_show: false,
      table_is_show: false
    },
    mounted: function() {
      var that;
      that = this;
      $.ajax({
        url: '/welcome/index.json',
        success: function(res) {
          that.indicator_types = res;
        }
      });
    },
    methods: {
      view_details: function (id){
        var that;
        that = this;
        $.ajax({
          url: '/welcome/get_indicator/'+id,
          success: function(res) {
            that.indicator_type_id = res.data.id;
            that.title = res.data.name;
            that.last_value = res.data.last_value;
            that.last_date = res.data.last_date;
            that.predict_value = res.data.predict_value;
            that.estimation_error = res.data.estimation_error;
            that.correlation_coefficient = res.data.correlation_coefficient;
            that.is_show = true;
            that.start_date = '';
            that.final_date = '';
            that.search_indicator_values = [];
            that.tabla_is_show = false;
            draw_chart(res.day_data);
          }
        });
      },
      view_between: function (id){
        var that;
        that = this;
        $.ajax({
          url: '/welcome/search/'+id+'/'+that.start_date+'/'+that.final_date,
          success: function(res) {
            that.search_indicator_values = res.data;
            that.tabla_is_show = true;
            draw_chart(res.day_data);
          }
        })
      }
    }
  });
};