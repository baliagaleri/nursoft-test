class IndicatorType < ApplicationRecord
  has_many :indicator_values, -> { order(value_date: :asc)}
end
