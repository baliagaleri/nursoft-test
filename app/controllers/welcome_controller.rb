class WelcomeController < ApplicationController

  before_action :set_indicator_type, :only => [:get_indicator, :search]

  def index
    # Carga todos los indicadores y sus valores más recientes
    @indicator_types = IndicatorType.all
    respond_to do |format|
      format.html
      format.json { render :json => indicator_types_json(@indicator_types) }
    end
  end

  def all_values_today
    # Obtiene todos los valores del día actual.
    today = Date.today.strftime('%d-%m-%Y')
    IndicatorType.all.each do |indicator_type|
      response = HTTParty.get("http://mindicador.cl/api/#{indicator_type.abbrev}/#{today}")
      if response.parsed_response['serie'].present? && indicator_type.indicator_values.where(:value_date => today).none?
        value = response.parsed_response['serie'][0]['valor']
        value_date = response.parsed_response['serie'][0]['fecha'].to_date.strftime('%d-%m-%Y')
        IndicatorValue.create(:indicator_type => indicator_type, :value => value, :value_date => value_date)
      end
    end

    redirect_to welcome_index_path
  end

  def test_values
    # Obtiene los valores del último mes del indicador consultado
    IndicatorType.all.each do |indicator_type|
      response = HTTParty.get("http://mindicador.cl/api/#{indicator_type.abbrev}")
      response.parsed_response['serie'].each do |serie|
        if indicator_type.indicator_values.where(:value_date => serie['fecha'].to_date.strftime('%d-%m-%Y')).none?
          value = serie['valor']
          value_date = serie['fecha'].to_date.strftime('%d-%m-%Y')
          IndicatorValue.create(:indicator_type => indicator_type, :value => value, :value_date => value_date)
        end
      end
    end

    redirect_to welcome_index_path
  end

  def get_indicator
    # Obtiene información de un indicador en específico
    predict_result = predict(@indicator_type, 7) # Días para calcular la predicción (1 Semana)
    data = {
      id: @indicator_type.id,
      name: @indicator_type.name,
      last_value: @indicator_type.indicator_values.last.value,
      last_date: @indicator_type.indicator_values.last.value_date.strftime('%d-%m-%Y'),
      predict_value: predict_result[:predict_value],
      estimation_error: predict_result[:estimation_error],
      correlation_coefficient: predict_result[:correlation_coefficient]
    }
    day_data = predict_chart(@indicator_type.indicator_values, 7) # Obtiene los valores para la predicción para ser graficados.
    render :json => { :data => data, :day_data => day_data }
  end

  def search
    # Realiza una busqueda entre fechas de los valores de los indicadores para ser mostrados
    # a través de una tabla y el gráfico
    start_date = params[:start_date].to_date.strftime('%d-%m-%Y')
    final_date = params[:final_date].to_date.strftime('%d-%m-%Y')
    data = @indicator_type.indicator_values.where(:value_date => start_date..final_date).map{|indicator_value|
      {
        :value => indicator_value.value,
        :value_date => indicator_value.value_date.strftime('%d-%m-%Y')
      }
    }
    day_data = predict_chart(@indicator_type.indicator_values.where(:value_date => start_date..final_date), 7) # Obtiene los valores para la predicción para ser graficados.
    render :json => { :data => data, :day_data => day_data }
  end

  def indicator_types_json(indicator_types)
    # Transforma la data para ser procesada por vue en tipo json
    indicator_types.map { |indicator_type|
      { id: indicator_type.id,
        name: indicator_type.name,
        last_value: indicator_type.indicator_values.last.blank? ? 'Sin valor registrado' : indicator_type.indicator_values.last.value,
        value_date: indicator_type.indicator_values.last.blank? ? '' : indicator_type.indicator_values.last.value_date.strftime('%d-%m-%Y')
      }
    }
  end

  private

  def set_indicator_type
    # Setea el indicador
    @indicator_type = IndicatorType.find(params[:id])
  end

  def average(values) # Calcula el promedio
    values.inject{ |sum, el| sum + el }.to_f / values.size
  end

  def predict_chart(indicator_values, day) # Realiza la predicción lineal y retorna la data necesaria para el gráfico
    data_y = indicator_values.map{|h| h['value']}
    data_x = []

    sum_x = 0
    sum_y = 0
    sum_xy = 0
    sum_x2 = 0
    sum_y2 = 0

    data_y.each.with_index(1) do |value, index|
      x = index
      y = value
      xy = x*y
      x2 = x**2
      y2 = y**2

      sum_x += x
      sum_y += y
      sum_xy += xy
      sum_x2 += x2
      sum_y2 += y2

      data_x.push(x)
    end

    average_x = average(data_x)
    average_y = average(data_y)

    b = inclination_line(sum_xy, data_x.size, average_x, average_y, sum_x2)
    a = secant_line(average_y, b, average_x)

    data_day = []

    indicator_values.each.with_index(1) do |indicator_value, index|
      day_name = indicator_value.value_date.strftime('%Y-%m-%d')
      value = indicator_value.value
      predict_value_two = predict_value(a, b,data_x.size, index)
      data_day.push({day: day_name, value: value, predict_value_two: predict_value_two})
    end

    day.times do |i|
      i += 1
      day_name = indicator_values.last.value_date + i
      value = 0
      predict_value_two = predict_value(a, b,data_x.size, i)
      data_day.push({day: day_name, value: value, predict_value_two: predict_value_two})
    end

    data_day
  end

  def predict(indicator_type, predict_day) # Método de predicción lineal simple

    data_y = indicator_type.indicator_values.map{|h| h['value']}
    data_x = []

    sum_x = 0
    sum_y = 0
    sum_xy = 0
    sum_x2 = 0
    sum_y2 = 0

    data_y.each.with_index(1) do |value, index|
      x = index
      y = value
      xy = x*y
      x2 = x**2
      y2 = y**2

      sum_x += x
      sum_y += y
      sum_xy += xy
      sum_x2 += x2
      sum_y2 += y2

      data_x.push(x)
    end

    average_x = average(data_x)
    average_y = average(data_y)

    b = inclination_line(sum_xy, data_x.size, average_x, average_y, sum_x2)
    a = secant_line(average_y, b, average_x)

    predict_value = predict_value(a, b, data_x.size, predict_day)
    estimation_error = estimation_error_value(sum_y2, a, sum_y, b, sum_xy, data_x.size)
    correlation_coefficient = correlation_coefficient_value(data_x, data_y, a, b)

    {:predict_value => predict_value, :estimation_error => estimation_error, :correlation_coefficient => correlation_coefficient}
  end

  def inclination_line(sum_xy, nx, average_x, average_y, sum_x2) # Inclinación de la línea
    (sum_xy - (nx * average_x * average_y)) / (sum_x2 - (nx * average_x ** 2))
  end

  def secant_line(average_y, b, average_x) # Lugar donde corta al eje y
    average_y - (b * average_x)
  end

  def predict_value(a, b, nx, day) # retorna el valor estimado
    (a + (b * nx + day)).round(2)
  end

  def estimation_error_value(sum_y2, a, sum_y, b, sum_xy, nx) # Retorna el valor de estimación de error
    numerator = sum_y2 - a * sum_y - b * sum_xy
    denominator = nx - 2
    Math.sqrt((numerator/denominator).to_f).round(2)
  end

  def correlation_coefficient_value(data_x, data_y, a, b) # Obtiene el coeficiente de correlación para ver que tan precisa es la predicción
    data_z = []
    data_yz = []
    data_y2 = []
    data_z2 = []

    data_x.each do |x|
      data_z.push(predict_value(a, b, data_x.size, x))
    end

    sum_y = 0
    sum_z = 0
    sum_yz = 0
    sum_y2 = 0
    sum_z2 = 0

    data_y.each_with_index do |y, index|
      z = data_z[index]

      yz = y*z
      y2 = y**2
      z2 = z**2

      sum_y += y
      sum_z += z
      sum_yz += yz
      sum_y2 += y2
      sum_z2 += z2

      data_yz.push(yz)
      data_y2.push(y2)
      data_z2.push(z2)
    end

    # Medias aritméticas de y - z
    mean_y = sum_y/data_y.size
    mean_z = sum_z/data_z.size

    # Covarianza
    cvz = (sum_yz/data_y.size) - (mean_y * mean_z)

    # Desviaciones Típicas
    dev_y = Math.sqrt((sum_y2/data_y.size) - mean_y**2)
    dev_z = Math.sqrt((sum_z2/data_z.size) - mean_z**2)

    # Coeficiente correlación lineal
    (cvz / (dev_y * dev_z)).round(2)

  end
end
