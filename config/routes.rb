Rails.application.routes.draw do
  get 'welcome/index'
  root 'welcome#index'
  post 'welcome/all_values_today'
  post 'welcome/test_values'
  get 'welcome/get_indicator/:id' => 'welcome#get_indicator', :as => 'welcome_get_indicator'
  get 'welcome/search/:id/:start_date/:final_date' => 'welcome#search', :as => 'welcome_search'
  get 'welcome/predict/:id' => 'welcome#predict', :as => 'welcome_predict'
end
