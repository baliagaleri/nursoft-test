class CreateIndicatorValues < ActiveRecord::Migration[5.1]
  def change
    create_table :indicator_values do |t|
      t.decimal :value
      t.date :value_date
      t.references :indicator_type, foreign_key: true

      t.timestamps
    end
  end
end
