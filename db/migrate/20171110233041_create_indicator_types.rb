class CreateIndicatorTypes < ActiveRecord::Migration[5.1]
  def change
    create_table :indicator_types do |t|
      t.string :name
      t.date :start_date

      t.timestamps
    end
  end
end
