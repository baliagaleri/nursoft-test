class AddAbbrevToIndicatorType < ActiveRecord::Migration[5.1]
  def change
    add_column :indicator_types, :abbrev, :string
  end
end
