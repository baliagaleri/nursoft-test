IndicatorValue.destroy_all
IndicatorType.destroy_all

ActiveRecord::Base.connection.reset_pk_sequence!('indicator_types')
ActiveRecord::Base.connection.reset_pk_sequence!('indicator_values')

IndicatorType.create([
                        {:name => 'Unidad de fomento (UF)', :start_date => '1977-01-01', :abbrev => 'uf'},
                        {:name => 'Libra de Cobre', :start_date => '2012-01-01', :abbrev => 'libra_cobre'},
                        {:name => 'Tasa de desempleo', :start_date => '2009-01-01', :abbrev => 'tasa_desempleo'},
                        {:name => 'Euro', :start_date => '1999-01-01', :abbrev => 'euro'},
                        {:name => 'Imacec', :start_date => '2004-01-01', :abbrev => 'imacec'},
                        {:name => 'Dólar observado', :start_date => '1984-01-01', :abbrev => 'dolar'},
                        {:name => 'Tasa Política Monetaria (TPM)', :start_date => '2001-01-01', :abbrev => 'tpm'},
                        {:name => 'Indice de valor promedio (IVP)', :start_date => '1990-01-01', :abbrev => 'ivp'},
                        {:name => 'Indice de Precios al Consumidor (IPC)', :start_date => '1928-01-01', :abbrev => 'ipc'},
                        {:name => 'Dólar acuerdo', :start_date => '1988-01-01', :abbrev => 'dolar_intercambio'},
                        {:name => 'Unidad Tributaria Mensual (UTM)', :start_date => '1990-01-01', :abbrev => 'utm'}
                     ])